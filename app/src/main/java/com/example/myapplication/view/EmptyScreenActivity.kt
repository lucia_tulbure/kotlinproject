package com.example.myapplication.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.myapplication.R
import com.example.myapplication.viewmodel.AppViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_empty_screen.*

@AndroidEntryPoint
class EmptyScreenActivity : AppCompatActivity() {
    private val infoViewModel: AppViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty_screen)
        infoViewModel.getInfo()
        infoViewModel.informationAboutSite.observe(this, Observer {
            it.information.let { infoList ->
                if (infoList != null) {
                    nr_total_users.text = infoList[0].total_users.toString()
                    nr_questions_per_minute.text = infoList[0].questions_per_minute.toString()
                    nr_answers_per_minute.text = infoList[0].answers_per_minute.toString()
                }

            }
        })
        questions_btn.setOnClickListener { startActivity(Intent(this, QuestionsActivity::class.java)) }
        users_btn.setOnClickListener { startActivity(Intent(this, UsersActivity::class.java)) }

    }
}