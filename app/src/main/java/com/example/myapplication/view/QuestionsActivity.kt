package com.example.myapplication.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.viewmodel.AppViewModel
import com.example.myapplication.R
import com.example.myapplication.models.Question
import com.example.myapplication.view.adapter.QuestionAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

@AndroidEntryPoint
class QuestionsActivity : AppCompatActivity() {
    private val questionViewModel: AppViewModel by viewModels()
    private lateinit var myAdapter: QuestionAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myAdapter = QuestionAdapter(this)
        recycler_view_questions.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@QuestionsActivity)
            adapter = myAdapter
        }
        questionViewModel.getQuestions()
        questionViewModel.questions.observe(this, Observer {
            it.questions.let { questionsFromApi ->
                if (questionsFromApi != null) {
                    myAdapter.setQuestions(questionsFromApi as ArrayList<Question>)
                    myAdapter.notifyDataSetChanged()
                }
            }
        })
    }
}