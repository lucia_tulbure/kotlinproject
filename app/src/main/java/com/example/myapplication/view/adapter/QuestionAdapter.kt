package com.example.myapplication.view.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.models.Question
import com.squareup.picasso.Picasso
import java.util.*

class QuestionAdapter(private val context: Context) :
        RecyclerView.Adapter<ViewHolder>() {
    private val questions: ArrayList<Question> = arrayListOf()
    fun setQuestions(newQuestions: ArrayList<Question>) {
        questions.clear()
        questions.addAll(newQuestions)
    }

    /**
     * Gets the count of items
     */
    override fun getItemCount(): Int {
        return questions.size
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_question,
                parent,
                false
            )
        )
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [ViewHolder.itemView] to reflect the item at the given
     * position.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val question: Question = questions[position]
        holder.bind(question)
    }
}


class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvQuestion: TextView = view.findViewById(R.id.question_tv)
    private val profileImage: ImageView = view.findViewById(R.id.profile_image)
    private val tagsTv : TextView = view.findViewById(R.id.tags_tv)
    fun bind(question: Question) {
        tvQuestion.text = question.title
        tagsTv.text = question.tags.joinToString(", ")
        Picasso.get().load(question.owner.profile_image).into(profileImage)
    }
}