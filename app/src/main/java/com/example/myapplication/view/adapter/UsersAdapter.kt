package com.example.myapplication.view.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.models.User
import com.squareup.picasso.Picasso
import java.util.*

class UsersAdapter(private val context: Context) :
    RecyclerView.Adapter<UsersViewHolder>() {
    private val users: ArrayList<User> = arrayListOf()
    fun setUsers(newUsers: ArrayList<User>) {
        users.clear()
        users.addAll(newUsers)
    }

    /**
     * Gets the count of items
     */
    override fun getItemCount(): Int {
        return users.size
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        return UsersViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_user,
                parent,
                false
            )
        )
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [ViewHolder.itemView] to reflect the item at the given
     * position.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val user: User = users[position]
        holder.bind(user)
    }
}


class UsersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvUser: TextView = view.findViewById(R.id.name_user_tv)
    private val profileImage: ImageView = view.findViewById(R.id.profile_image_user)
    private val locationTv: TextView = view.findViewById(R.id.location_tv)
    fun bind(user: User) {
        tvUser.text = user.display_name
        locationTv.text = user.location
        Picasso.get().load(user.profile_image).into(profileImage)
    }
}