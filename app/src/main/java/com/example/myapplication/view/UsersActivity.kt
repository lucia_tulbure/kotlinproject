package com.example.myapplication.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.viewmodel.AppViewModel
import com.example.myapplication.R
import com.example.myapplication.models.User
import com.example.myapplication.view.adapter.UsersAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_users.*
import java.util.*

@AndroidEntryPoint
class UsersActivity : AppCompatActivity() {
    private val usersViewModel: AppViewModel by viewModels()
    private lateinit var myAdapter: UsersAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        myAdapter = UsersAdapter(this)
        recycler_view_users.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@UsersActivity)
            adapter = myAdapter
        }
        usersViewModel.getUsers()
        usersViewModel.usersList.observe(this, androidx.lifecycle.Observer {
            it.users.let { usersFromApi ->
                if (usersFromApi != null) {
                    myAdapter.setUsers(usersFromApi as ArrayList<User>)
                    myAdapter.notifyDataSetChanged()
                }
            }
        })
    }
}