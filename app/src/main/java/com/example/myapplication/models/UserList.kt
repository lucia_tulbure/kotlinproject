package com.example.myapplication.models

import com.google.gson.annotations.SerializedName

class UserList {
    @SerializedName("items")
    val users: List<User>? = null
}

data class User(
    val display_name: String,
    val location: String,
    val profile_image: String
)
