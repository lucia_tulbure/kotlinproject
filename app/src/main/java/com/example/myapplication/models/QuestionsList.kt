package com.example.myapplication.models

import com.example.myapplication.models.Question
import com.google.gson.annotations.SerializedName

class QuestionsList {
    @SerializedName("items")
    val questions: List<Question>? = null
}