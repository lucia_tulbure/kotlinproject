package com.example.myapplication.models

import com.google.gson.annotations.SerializedName

class InfoList {
    @SerializedName("items")
    val information: List<Info>? = null
}

data class Info(
    val total_users: Long,
    val answers_per_minute: Float,
    val questions_per_minute: Float
)

