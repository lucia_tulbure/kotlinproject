package com.example.myapplication.models

data class Question(val title: String, val owner: Owner, val tags: List<String>)
