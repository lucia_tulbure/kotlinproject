package com.example.myapplication.retrofit

import com.example.myapplication.models.InfoList
import com.example.myapplication.models.QuestionsList
import com.example.myapplication.models.UserList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface QuestionApiService {
    @GET("questions")
    suspend fun getQuestionsList(@Query("site") site: String): Response<QuestionsList>
    @GET("info")
    suspend fun getInfoAboutSiteList(@Query("site") site: String): Response<InfoList>
    @GET("users")
    suspend fun getUsersList(@Query("site") site: String): Response<UserList>
}