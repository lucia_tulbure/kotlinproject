package com.example.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.repository.Repository
import com.example.myapplication.models.InfoList
import com.example.myapplication.models.QuestionsList
import com.example.myapplication.models.UserList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AppViewModel
@Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val mutableQuestionsList = MutableLiveData<QuestionsList>()
    val questions: LiveData<QuestionsList> = mutableQuestionsList

    fun getQuestions() = viewModelScope.launch {
        mutableQuestionsList.value = repository.getQuestions().body()
    }

    private val mutableInfoList = MutableLiveData<InfoList>()
    val informationAboutSite: LiveData<InfoList> = mutableInfoList

    fun getInfo() = viewModelScope.launch {
        mutableInfoList.value = repository.getInfo().body()
    }

    private val mutableUserList = MutableLiveData<UserList>()
    val usersList: LiveData<UserList> = mutableUserList

    fun getUsers() = viewModelScope.launch {
        mutableUserList.value = repository.getUsers().body()
    }
}