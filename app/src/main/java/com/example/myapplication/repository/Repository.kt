package com.example.myapplication.repository

import com.example.myapplication.helpers.Constants.SITE
import com.example.myapplication.retrofit.QuestionApiService
import javax.inject.Inject

class Repository @Inject constructor(
    private val api: QuestionApiService
) {
    suspend fun getQuestions() = api.getQuestionsList(SITE)
    suspend fun getInfo() = api.getInfoAboutSiteList(SITE)
    suspend fun getUsers() = api.getUsersList(SITE)
}