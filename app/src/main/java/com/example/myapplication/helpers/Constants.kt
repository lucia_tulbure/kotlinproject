package com.example.myapplication.helpers

object Constants {
    const val BASE_URL = "https://api.stackexchange.com/2.2/"
    const val SITE = "stackoverflow"
}
